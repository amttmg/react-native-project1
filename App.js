import React, {Component} from "react";
import {StyleSheet, View} from "react-native";
import {Provider} from 'react-redux';
import configStore from './src/store/configStore';
import MainComponent from './MainComponent';

const store = configStore();


class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <MainComponent/>
      </Provider>
    );
  }
}


export default App;