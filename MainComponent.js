import React, {Component} from "react";
import {StyleSheet, View} from "react-native";
import {connect} from 'react-redux';

import PlaceInput from "./src/components/PlaceInput/PlaceInput";
import PlaceList from "./src/components/PlaceList/PlaceList";
import PlaceDetail from './src/components/PlaceDetails/PlaceDetail';
import {addPlace, deSelectPlace, deletePlace, selectPlace} from "./src/store/actions/index";

class MainComponent extends Component {
  placeAddedHandler = placeName => {
    this.props.onAddPlace(placeName);
  };
  placeSelectedHandler = key => {
    this.props.onSelectPlace(key);
  };
  placeDeletedHandler = () => {
    this.props.onDeletePlace();
  };
  modelClosedHandler = () => {
    this.props.onDeSelectPlace();
  };

  render() {
    return (
      <View style={styles.container}>
        <PlaceDetail
          onItemDeleted={this.placeDeletedHandler}
          selectedPlace={this.props.selectedPlace}
          onModelClosed={this.modelClosedHandler}
        />
        <PlaceInput onPlaceAdded={this.placeAddedHandler}/>
        <PlaceList
          places={this.props.places}
          onItemSelected={this.placeSelectedHandler}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 40,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "flex-start"
  }
});
const mapStateToProps = state => {
  return {
    places: state.places.places,
    selectedPlace: state.places.selectedPlace
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onAddPlace: (name) => dispatch(addPlace(name)),
    onDeletePlace: () => dispatch(deletePlace()),
    onSelectPlace: (key) => dispatch(selectPlace(key)),
    onDeSelectPlace: () => dispatch(deSelectPlace()),
  };
};


export default connect(mapStateToProps, mapDispatchToProps)(MainComponent);