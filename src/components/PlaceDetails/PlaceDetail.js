import React from 'react';
import {Modal, View, Image, Button, Text, StyleSheet} from "react-native";


const PlaceDetail = (props) => {
  let modelContent = null;
  if (props.selectedPlace) {
    modelContent = ( <View>
        <Image source={props.selectedPlace.fullImage} style={styles.image}/>
        <Text style={styles.placeName}>{props.selectedPlace.name}</Text>
      </View>
    )
  }
  return (
    <Modal visible={props.selectedPlace !== null} animationType="slide" onRequestClose={props.onModelClosed}>
      <View style={styles.modelContainer}>
        {modelContent}
        <View>
          <Button title="Delete" color="red" onPress={props.onItemDeleted}/>
          <Button title="Close" onPress={props.onModelClosed}/>
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  modelContainer: {
    margin: 22,
  },
  image: {
    height: 200,
    width: '100%',
  },
  placeName: {
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 28,

  }
});

export default PlaceDetail;